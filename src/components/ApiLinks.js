export var linklcall = {
  method: "POST",
  hostname: ["app.linkl.com.br"],
  path: ["/apis/apiLigueme.php"],
  headers: {}
};

export var linklmsg = {
  method: "POST",
  hostname: ["app.linkl.com.br"],
  path: ["/apis/apiMensagem.php"],
  headers: {}
};

export var linklwhats = {
  method: "POST",
  hostname: ["app.linkl.com.br"],
  path: ["/apis/apiWhatsApp.php"],
  headers: {}
};

export var validFunc = {
  method: "POST",
  hostname: ["app.linkl.com.br"],
  path: ["/apis/apiValidacoesWidget.php"],
  headers: {}
};

export var userStatusVal = {
  method: "POST",
  hostname: ["app.linkl.com.br"],
  path: ["/apis/apiStatusCliente.php"],
  headers: {}
};

export var options = {
  method: "GET",
  hostname: ["worldclockapi.com"],
  path: ["/api/json/utc/now"],
  headers: {}
};
