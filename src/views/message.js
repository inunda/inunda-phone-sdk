import http from "http";
import validator from "validator";
import {
  linklcall,
  linklmsg,
  linklwhats,
  validFunc,
  userStatusVal,
  options
} from "../components/ApiLinks";

// Funcao principal
export function show() {
  // Criação do elemento com shadow dom
  class linkl extends HTMLElement {
    constructor() {
      super();
      const shadowRoot = this.attachShadow({ mode: "open" });
      shadowRoot.innerHTML = `
        <style>
        @import url('https://fonts.googleapis.com/css?family=Open+Sans&display=swap');
          #inunda-form {
            width: 460px;
            height: 520px;
            border-radius: 19px;
            box-shadow: 0 0 44px 0 rgba(0, 0, 0, 0.08);
            background-color: #ffffff;
            display: block;
            justify-items: center;
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            z-index: 9999;
            padding: 15px 70px;
          }
          #inunda-one {
            display: grid;
            grid-row-gap: 16px;
            transition: 0.3s linear;
          }
          #inunda-two {
            display: none;
            grid-row-gap: 16px;
            transition: 0.3s linear;
          }
          #inunda-three {
            display: none;
            grid-row-gap: 16px;
            transition: 0.3s linear;
          }
          #inunda-five {
            display: none;
            grid-row-gap: 16px;
            transition: 0.3s linear;
            grid-template-columns: 100%;
          }
          input {
            width: 458.7px;
            border-radius: 12px;
            box-shadow: 0 4px 16px 0 rgba(69, 91, 99, 0.08);
            border: solid 1px #c6c6c6;
            background-color: #ffffff;
            font-size: 14px;
            padding: 15px 12px;
            width: auto;
            outline: none;
            resize: none;
            font-family: 'Open Sans', sans-serif;
          }
          textarea {
            border-radius: 12px;
            box-shadow: 0 4px 16px 0 rgba(69, 91, 99, 0.08);
            border: solid 1px #c6c6c6;
            background-color: #ffffff;
            font-size: 14px;
            padding: 12px;
            width: auto;
            outline: none;
            resize: none;
            font-family: 'Open Sans', sans-serif;
          }
          input::placeholder {
            font-family: Lato;
            font-size: 18px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: 1.22;
            letter-spacing: 0.4px;
            text-align: left;
            color: #333333;
          }
          #inunda-submitmsg,
          #inunda-submitagendar,
          #inunda-submitwhats {
            width: 327px;
            height: 33px;
            border-radius: 12px;
            background-color: #665eff;
            border: none;
            color: #FFF;
            cursor: pointer;
            font-family: 'Open Sans', sans-serif;
            font-size: 16px;
            font-weight: 600;
            padding: 12px 32px;
            text-align: center;
            text-transform: uppercase;
            position: relative;
            left: 50%;
            transform: translateX(-50%);
            margin: 0 0 18px;
            overflow: hidden;
            transform-origin: 50% 50%;
          }
          #inunda-submitmsg {
            margin-top: -12px;
          }
          #inunda-submitcall {
            width: 327px;
            height: 33px;
            border-radius: 12px;
            background-color: #665eff;
            border: none;
            color: #FFF;
            cursor: pointer;
            font-family: 'Open Sans', sans-serif;
            font-size: 16px;
            font-weight: 600;
            padding: 12px 32px;
            text-align: center;
            text-transform: uppercase;
            position: relative;
            left: 50%;
            transform: translateX(-50%);
            margin: 18px 0;
            overflow: hidden;
            transform-origin: 50% 50%;
          }
          #inunda-submitmsg::before,
          #inunda-submitagendar::before,
          #inunda-submitcall::before,
          #inunda-submitwhats::before {
            content: "";
            position: absolute;
            width: 1px;
            height: 1px;
            background: #5145d3;
            border-radius: 100%;
            transform-origin: 50% 50%;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            z-index: 0;
          }
          #inunda-submitmsg:hover::before,
          #inunda-submitagendar:hover::before,
          #inunda-submitcall:hover::before,
          #inunda-submitwhats:hover::before {
            animation: icons 1.4s;
          }
          @keyframes icons {
            from {
                transform: scale(0)
            } to {
                transform: scale(2000)
            }
          }
          #inunda-submitcall-text {
            position: absolute;
            transition: 0.2s linear;
            left: 50%;
            top: 25%;
            transform: translate(-50%, -50%);
          }
          #inunda-submitcall-text:hover {
            z-index: 999;
          }
          p#inunda-close {
            cursor: pointer;
            position: absolute;
            right: 3px;
            top: -35px;
            width: 38px;
            height: 38px;
            opacity: 0.67;
            font-size: 38px;
            font-weight: 300;
            font-style: normal;
            font-stretch: normal;
            line-height: 1.21;
            letter-spacing: normal;
            text-align: left;
            color: #707070;
            padding: 5px;
            opacity: 0.2;
            transition: opacity 0.2s cubic-bezier(.17,.84,.44,1)
          }
          p#inunda-close:hover {
            background: #26C1C0;
            filter: invert(1);
            padding: 5px;
            border-radius: 100%;
            opacity: 1;
          }
          p#inunda-title {
            font-family: 'Open Sans', sans-serif;
            font-size: 26px;
            font-weight: bold;
            font-style: normal;
            font-stretch: normal;
            line-height: 1.39;
            letter-spacing: normal;
            text-align: center;
            color: #000000;
            margin-bottom: 20px;
            margin-top: 20px;
          }
          #inunda-tabs {
            border: none;
            border-bottom: 1px solid #c7c7c7;
            display: flex;
            list-style: none;
            text-align: center;
            justify-content: center;
            margin: 0;
            padding: 0;
          }
          #inunda-tabone { 
            padding: 12px;
            font-family: 'Open Sans', sans-serif;
          }
          #inunda-tabtwo { 
            padding: 12px;
            font-family: 'Open Sans', sans-serif;
          }
          #inunda-tabthree {
            padding: 12px;
            font-family: 'Open Sans', sans-serif;
          }
          #inunda-tabfive {
            padding: 12px;
            font-family: 'Open Sans', sans-serif;
          }
          #inunda-tafive i {
            border-radius: 100%;
            box-shadow: 0 3px 6px 0 rgba(254, 30, 154, 0.3);
            background-image: linear-gradient(-135deg, #fe1e9a, #254dde);
            color: #FFF;
            padding: 22px;
            cursor: pointer;
            font-size: 25px;
          }
          #inunda-tabone i {
            border-radius: 100%;
            box-shadow: 0 3px 6px 0 rgba(254, 30, 154, 0.3);
            background-image: linear-gradient(-135deg, #fe1e9a, #254dde);
            color: #FFF;
            padding: 22px;
            cursor: pointer;
            font-size: 25px;
          }
          #inunda-tabtwo i {
            background-color: #fe577a;
            box-shadow: 0px 3px 6px #3131317D;
            color: #FFF;
            padding: 22px;
            border-radius: 100%;
            cursor: pointer;
            font-size: 25px;
          }
          #inunda-tabthree i {
            border-radius: 100%;
            box-shadow: 0 3px 6px 0 rgba(0, 255, 255, 0.3);
            background-image: linear-gradient(214deg, #00ffff, #254dde);
            color: #FFF;
            padding: 22px;
            cursor: pointer;
            font-size: 25px;
          }
          #inunda-tabone p {
            font-size: 14px;
            font-weight: 600;
            text-align: center;
            color: #696969;
          }
          #inunda-tabtwo p {
            font-size: 14px;
            font-weight: 600;
            text-align: center;
            color: #696969;
          }
          #inunda-tabthree p {
            font-size: 14px;
            font-weight: 600;
            text-align: center;
            color: #696969;
          }
          #inunda-overlay {
            background: #313131ad;
            position: fixed;
            top: 0;
            left: 0;
            width: 100vw;
            height: 100vh;
            z-index: 999;
          }
          #inunda-icon {
            border-radius: 100%;
            box-shadow: 0 3px 6px 0 rgba(254, 30, 154, 0.3);
            background-image: linear-gradient(-135deg, #fe1e9a, #254dde);
            padding: 42px;
            transform: scale(0.8);
            position: absolute;
            bottom: 18px;
            right: 18px;
            cursor: pointer;
            overflow: hidden;
            z-index: 1;
          }
          #inunda-phoneicon {
            border-radius: 100%;
            box-shadow: 0 3px 6px 0 rgba(254, 30, 154, 0.3);
            background-image: linear-gradient(-135deg, #fe1e9a, #254dde);
            color: #FFF;
            padding: 12px;
            cursor: pointer;
            font-size: 25px;
            display: inline-flex;
            height: 25px;
            margin: 0 auto;
          }
          #inunda-clockicon {
            background-image: linear-gradient(214deg, #ff7b97, #9f2a2a);
            box-shadow: 0px 3px 6px #3131317D;
            color: #FFF;
            padding: 12px;
            border-radius: 100%;
            cursor: pointer;
            font-size: 20px;
            display: inline-flex;
            height: 25px;
            margin: 0 auto;
          }
          #inunda-msgicon {
            border-radius: 100%;
            box-shadow: 0 3px 6px 0 rgba(0, 255, 255, 0.3);
            background-image: linear-gradient(214deg, #00ffff, #254dde);
            color: #FFF;
            padding: 12px;
            cursor: pointer;
            font-size: 20px;
            width: 25px;
            height: 25px;
            margin: 0 auto;
          }

          #inunda-msgicon {
            border-radius: 100%;
            box-shadow: 0 3px 6px 0 rgba(0, 255, 255, 0.3);
            background-image: linear-gradient(214deg, #00ffff, #254dde);
            color: #FFF;
            padding: 12px;
            cursor: pointer;
            font-size: 20px;
            width: 25px;
            height: 25px;
            margin: 0 auto;
          }

          #inunda-whatsicon {
            border-radius: 100%;
            box-shadow: 0 3px 6px 0 rgba(0, 255, 255, 0.3);
            background-image: linear-gradient(214deg, #2ed543, #1b8e2a);
            color: #FFF;
            padding: 12px;
            cursor: pointer;
            width: 25px;
            height: 25px;
            margin: 0 auto;
            font-size: 20px;
          }

          #inunda-tabfive p {
            font-size: 14px;
            font-weight: 600;
            text-align: center;
            color: #696969;
        }

          #inunda-phoneicon img {
            filter: invert(1);
            width: 25px;
          }
          #inunda-clockicon img {
            filter: invert(1);
            width: 25px;
          }
          #inunda-msgicon img {
            filter: invert(1);
            width: 25px;
          }

          #inunda-whatsicon img {
            filter: invert(1);
            width: 25px;
          }

          #master-icon {
            animation: 1s iconreveal cubic-bezier(.68,-0.55,.27,1.55);
            position: fixed;
            width: 125px;
            height: 125px;
            bottom: 0;
            right: 0px;
            transform-origin: 50% 50%;
            z-index: 9999;
        }

          @keyframes iconreveal {
            from {
              bottom: -85px;
            } to {
              bottom: 18px;
            }
          }
          #inunda-icon-pulse {
            content: '';
              position: absolute;
              width: 50px;
              height: 50px;
              background: #254DDE;
              border-radius: 100%;
              right: 35px;
              bottom: 35px;
              transform-origin: 50% 50%;
              animation: 1.2s infinite pulseicon cubic-bezier(0,0,.22,1.03);
              opacity: 1;
              animation-delay: 1s;
          }
          @keyframes pulseicon {
            from {
              transform: scale(1);
              opacity: 1;
            } to {
              transform: scale(2);
              opacity: 0;
            }
          }
          #inunda-grid {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            grid-template-rows: repeat(3, auto);
            grid-gap: 10px;
            margin: 0 0 18px;
          }
          #inunda-grid #inunda-namemsg {
            grid-column: 1;
            grid-row: 1;
          }
          #inunda-grid #inunda-emailmsg {
            grid-column: 1;
            grid-row: 3;
          }
          #inunda-grid #inunda-phonemsg {
            grid-column: 1;
            grid-row: 2;
          }
          #inunda-grid #inunda-msgmsg {
            grid-column: 2;
            grid-row: 1/4;
          }
          #text-bottom {
            font-family: Lato;
            font-size: 15px;
            font-weight: 300;
            font-style: normal;
            font-stretch: normal;
            line-height: 1.2;
            letter-spacing: normal;
            text-align: center;
            color: #181743;
            margin-top: 0;
          }
          #agendar-grid {
            display: grid;
            grid-template-columns: repeat(2,1fr);
            grid-template-rows: repeat(2, auto);
            grid-gap: 15px;
            margin: 0 0 18px;
          }
          #agendar-grid #inunda-nameng {
            grid-column: 1;
            grid-row: 1;
          }
          #agendar-grid #inunda-phoneng {
            grid-column: 2;
            grid-row: 1;
          }
          #agendar-grid #inunda-dateng {
            grid-column: 1;
            grid-row: 2;
            border-radius: 12px;
            box-shadow: 0 4px 16px 0 rgba(69, 91, 99, 0.08);
            border: solid 1px #c6c6c6;
            background-color: #ffffff;
            font-size: 14px;
            padding: 15px 12px;
            width: auto;
            outline: none;
            resize: none;
            font-family: 'Open Sans', sans-serif;
            color: #9191a3;
            cursor: pointer;
          }
          #agendar-grid #inunda-hourng {
            grid-column: 2;
            grid-row: 2;
          }
          #arrow-up {
            width: 0px;
            height: 0px;
            border-left: 15px solid transparent;
            border-right: 15px solid transparent;
            border-bottom: 20px solid #c7c7c7;
            transform: translate(-50%, -50%);
            position: absolute;
            top: 50%;
            left: 50%;
          }
          #arrow-up--inside {
            width: 0;
            height: 0;
            border-left: 15px solid transparent;
            border-right: 15px solid transparent;
            border-bottom: 20px solid #FFF;
            transform: translate(-50%, 10%);
            position: absolute;
            top: 50%;
            left: 50%;
          }
          #trimastercall, #trimastermsg, #trimasteragenda, #trimasterwhats  {
            position: relative;
            width: 20px;
            height: 20px;
            bottom: -12px;
            left: 0; 
            right: 0;
            margin: -20px auto 0;
            display:none;
          }
          #sprite {
            display: flex;
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
            animation: sprite 3.5s 0s infinite cubic-bezier(0,1.12,.09,1.27);
          }

          @keyframes sprite {
            0% { left: 60px }
            20% { left: -3px }
            30% { left: -3px }
            40% { left: -95px }
            50% { left: -95px }
            60% { left: -188px }
            70% { left: -188px }
            80% { left: -278px }
            90% { left: -278px }
            100% { left: -344px }
          }

          #inunda-iconbtn{
            width: 42px;
            filter: invert(1);
            margin: 0 25px;
          }

          .check {
            height: 320px;
            left: 45%;
            position: absolute;
            top: 58%;
            width: 320px;
            transform: translate(-50%, -50%);
          }
          .check-circle {
            background: transparent;
            border: 6px solid #74b196;
            border-radius: 100%;
            height: 162px;
            left: 58%;
            position: absolute;
            top: 50%;
            transform: translate(-50%, -50%);
            width: 162px;
            animation: checkcircle 2s 0.5s linear;
            opacity: 0;
          }

          @keyframes checkcircle {
            from{
              transform: scale(0.1), translate(-50%, -50%);
              opacity: 0;
            } to {
              transform: scale(0.5), translate(-50%, -50%);
              opacity: 1;
            }
          }

          .check-arrow {
            transform: scale(5), translate(-50%, -50%);
            position: absolute;
            left: 30%;
            top: 24%;
            animation: checkarrow 2s 0.5s linear;
            opacity: 0;
          }

          @keyframes checkarrow {
            from{
              opacity: 0;
            } to {
              opacity: 1;
            }
          }

          #inunda-datelist {
            background: #fff;
            border: none;
            border-radius: 12px;
            display: block;
            text-align: center;
            position: absolute;
            z-index: 999;
            height: 170px;
            width: 219px;
            left: 12%;
            overflow: hidden;
            bottom: 23px;
            box-shadow: 0px 6px 8px #313131ad;
            display: none;
          }

          #inunda-datelistscroll {
            border: none;
            border-radius: 12px;
            display: block;
            text-align: center;
            position: absolute;
            z-index: 999;
            height: 170px;
            width: 256px;
            left: 0;
            overflow: hidden;
            overflow-y: scroll;
            bottom: 0px;
          }

          #inunda-datelist p {
            padding: 8px;
            position: relative;
            left: -19px;
            font-family: 'Open Sans', sans-serif;
            cursor: pointer;
            width: 148px;
            left: 45%;
            transform: translateX(-50%);
            border-radius: 12px;
          }

          #inunda-datelist p:hover {
            background: #5145d3;
            color: #f1f1f1;
          }

          #inunda-msg {
            font-family: 'Open Sans', sans-serif;
            font-size: 14px;
          }

          #inunda-msg::placeholder {
            font-family: 'Open Sans', sans-serif;
            font-size: 14px;
          }

          #inunda-four p {
            font-size: 32px;
            font-family: 'Open Sans', sans-serif;
            color: #74b196;
            text-align: center;
            width: 352px;
            position: absolute;
            top: 62%;
            left: 50%;
            transform: translate(-50%, -50%);
            opacity: 0;
            animation: checkarrow 2s 0.5s linear;
          }

          #inunda-four img {
            font-size: 18px;
            font-family: 'Open Sans', sans-serif;
            color: #74b196;
            text-align: center;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            opacity: 0;
            animation: checkarrow 2s 0.5s linear;
          }

          #logo-link{
            position: absolute;
            left: 50%;
            transform: translateX(-50%);
            bottom: -25px;
            width: 58px;
            z-index: 9999;
          }

          #tooltip {
            background: #f1f1f1;
            position: fixed;
            padding: 0 32px;
            border: none;
            border-radius: 12px 12px 0px 12px;
            box-shadow: 0px 6px 8px #31313132;
            bottom: 30px;
            right: 128px;
            display: none;
            z-index: 9999;
          }

          #close-tooltip {
            font-family: 'Open sans', sans-serif;
            position: absolute;
            color: #cc1618;
            box-shadow: 0px 6px 8px #31313132;
            padding: 4px 10px;
            border-radius: 100%;
            padding-top: 2px;
            background: #eaeaea;
            border: none;
            right: -12px;
            top: -24px;
            cursor: pointer;
          }

          #inunda-datearrow {
            font-family:  'Open Sans', sans-serif;
            position: absolute;
            left: 44%;
            top: 60%;
            cursor: pointer;
          }

          #msg-tooltip {
            font-family: 'Open sans', sans-serif;
            font-size: 16px;
          }

          @media screen and (max-width: 600px) {
            #inunda-form {
              width: 98% !important;
              height: 512.6px;
              box-sizing: border-box;
              padding: 15px 20px; 
            }
            #inunda-four p {
              font-size: 24px;
            }
            #agendar-grid{ 
              grid-template-columns: repeat(2,48%);
            }
            #trimaster { 
              left: 10px; 
            }
            #inunda-two {
              grid-template-columns: 100%;
              position: relative;
            }
            #agendar-grid {
              grid-template-columns: repeat(2,48%);
            }
            #inunda-three {
              grid-template-columns: 100%;
            }
            #inunda-grid {
              grid-template-columns: repeat(2, 48%);
            }
            #inunda-one {
              grid-template-columns: 100%;
            }
            #inunda-submitcall, #inunda-submitmsg, #inunda-submitagendar {
              width: 60%;
            }
            #inunda-submitwhats {
              width: 60%;
            }
            p#inunda-close {
              position: absolute;
              right: 0px;
              width: 30px;
              top: -38px;
            }
            #inunda-submitcall {
              margin: 0;
              top: 10px;
            }
            p#inunda-title {
              font-size: 20px;
              margin-bottom: 0;
            }
            #text-bottom {
              margin-top: 16px;
            }
            #logo-link{
              position: absolute;
              left: 50%;
              transform: translateX(-50%);
              bottom: -22px;
              width: 58px;
            }
            #about-linkl {
              display: none;
            }
            #msg-tooltip {
              font-family: 'Open sans', sans-serif;
              font-size: 12px;
            }
            #tooltip {
              right: 121px;
            }
          } 
        </style>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css" />
        <div id="master-icon">
          <div id="inunda-icon">
            <div id="sprite">
              <img id="inunda-iconbtn" src="http://linkl.com.br/widget/img/phone-volume.svg" />
              <img id="inunda-iconbtn" src="http://linkl.com.br/widget/img/clock.svg" />
              <img id="inunda-iconbtn" src="http://linkl.com.br/widget/img/comment-alt-lines.svg" />
              <img id="inunda-iconbtn" src="http://linkl.com.br/widget/img/whatsapp-logo.svg" />
            </div>
          </div>
          <div id="inunda-icon-pulse"></div>
        </div>
        <div id="inunda-overlay"></div>
        <form id="inunda-form" onsubmit="return false">
          <p id="inunda-close">
            <img src="http://linkl.com.br/widget/img/times-circle.svg">
          </p>
          <ul id="inunda-tabs">
            <li id="inunda-tabone">
              <div id="inunda-phoneicon">
                <img src="http://linkl.com.br/widget/img/phone-volume.svg" />
              </div>
              <p>Ligação</p>
              <div id="trimastercall">
                <div id="arrow-up">
                <div id="arrow-up--inside"></div>
              </div>
            </li>
            <li id="inunda-tabtwo">
              <div id="inunda-clockicon">
                <img src="http://linkl.com.br/widget/img/clock.svg" />
              </div>
              <p>Agendar ligação</p>
              <div id="trimastermsg">
                <div id="arrow-up">
                <div id="arrow-up--inside"></div>
              </div>
            </li>
            <li id="inunda-tabthree">
              <div id="inunda-msgicon">
                <img src="http://linkl.com.br/widget/img/comment-alt-lines.svg" />
              </div>
                <p>Mensagem</p>
                <div id="trimasteragenda">
                  <div id="arrow-up">
                  <div id="arrow-up--inside"></div>
                </div>
            </li>
            <li id="inunda-tabfive">
              <div id="inunda-whatsicon">
                <img src="http://linkl.com.br/widget/img/whatsapp-logo.svg" />
              </div>
                <p>WhatsApp</p>
                <div id="trimasterwhats">
                  <div id="arrow-up">
                  <div id="arrow-up--inside"></div>
                </div>
            </li>
          </ul>
          
          </div>
          <div id="inunda-one">
            <p id="inunda-title">
              Receba uma ligação em até 28 segundos.
            </p>
            <input id="inunda-name" type="text" placeholder="Nome" required />
            <input id="inunda-phone" type="text" pattern="[0-9]{2}-[0-9]{5}-[0-9]{4}" placeholder="Telefone" required/>
            <!--<input type="submit" value="Ligar" />-->
            <a id="inunda-submitcall">
              <p id="inunda-submitcall-text">Ligar</p>
            </a>
            <p id="text-bottom">
              Caso todas as linhas estiverem ocupadas, retornaremos seu contato!
            </p>
          </div>
          <div id="inunda-two">
            <p id="inunda-title">
              Estamos fora do expediente agende sua ligação.
            </p>
            <div id="agendar-grid">
              <input id="inunda-nameng" type="text" placeholder="Nome" />
              <input id="inunda-phoneng" type="text" placeholder="Telefone" />
              <input id="inunda-dateng" type="text" placeholder="Data" value="Agendar dia" readonly />
              <div id="inunda-datelist">
                <div id="inunda-datelistscroll">
                  <p id="todayDate">Hoje</p>
                  <p id="tomorrowDate"></p>
                  <p id="dayOne"></p>
                  <p id="dayTwo"></p>
                </div>
              </div>
              <input id="inunda-hourng" type="time" placeholder="Hora" />
            </div>
            <!--<input id="inunda-submitagendar" type="submit" value="Agendar" />-->
            <a id="inunda-submitagendar">
              <p id="inunda-submitcall-text">Agendar</p>
            </a>
          </div>

          <div id="inunda-three">
          <p id="inunda-title">
              Deixe sua mensagem e retornaremos em breve.
            </p>
            <div id="inunda-grid">
              <input id="inunda-namemsg" type="text" placeholder="Nome" />
              <input id="inunda-phonemsg" type="text" placeholder="Telefone" />
              <input id="inunda-emailmsg" type="email" placeholder="Email" />
            <textarea id="inunda-msgmsg" placeholder="Mensagem"></textarea>
            </div>
            <!--<input id="inunda-submitmsg" type="submit" value="Enviar" />-->
            <a id="inunda-submitmsg">
              <p id="inunda-submitcall-text">Enviar</p>
            </a>
          </div>

          <div id="inunda-five">
            <p id="inunda-title">
              Envie uma mensagem através do Whatsapp.
            </p>
            <input id="inunda-phonewhats" type="text" placeholder="Telefone" />
            <textarea id="inunda-msgwhats" placeholder="Mensagem"></textarea>
            <a id="inunda-submitwhats">
              <p id="inunda-submitcall-text">Enviar</p>
            </a>
          </div>

          <div id="inunda-four">
            <img src="http://linkl.com.br/widget/img/check-circle.png" />
            <p>Solicitado com sucesso!</p>
          </div>

          <a id="about-linkl" target="_blank" rel=”noopener” href="http://linkl.com.br">
            <img id="logo-link" src="http://linkl.com.br/widget/img/logo-linkl.png" />
          </a>

        </form>

        <div id="tooltip">
          <p id="close-tooltip">x</p>
          <p id="msg-tooltip">Receba uma ligação em 28s!</p>
        </div>
      `;
    }
  }
  // Definir o elemento personalisado que vai ser o pai da aplicação
  customElements.define("linkl-app", linkl);

  //Variaveis do widget
  let globalObject = window[window["linkl-widget"]];
  let queue = globalObject.q;

  if (queue) {
    for (var i = 0; i < queue.length; i++) {
      var userinfo = queue[i];
    }
    var pluginUser = userinfo[0];
    var pluginToken = userinfo[1];

    if (validator.isMobilePhone(userinfo[2])) {
      var pluginWhats = userinfo[2];
    } else {
      var pluginWhats = "";
    }
    var pluginUrl = userinfo[3];
  }

  window.addEventListener("load", function() {
    var req = http.request(userStatusVal, function(res) {
      var chunks = [];
      var newStatusDash;
      res.on("data", function(chunk) {
        chunks.push(chunk);
        var body = Buffer.concat(chunks);
        var statusFromDash = body.toString();
        newStatusDash = JSON.parse(statusFromDash);
        recebe(newStatusDash);
      });
      res.on("end", function() {});
    });

    req.write(JSON.stringify({ token: pluginToken }));
    req.end();
  });

  function recebe(initdois) {
    var statusUser = initdois;

    if (statusUser.status === "bloqueado") {
      console.log("BLOQUEADO");
    } else {
      // Cria o elemento que envolve o elemento shadow dom
      const getCustomTag = document.createElement("linkl-app");
      getCustomTag.id = "inunda";
      document.body.appendChild(getCustomTag);

      // Tela de confirmação por Default vem como display none
      let tabFour = inunda.shadowRoot.querySelector("#inunda-four");
      tabFour.style.display = "none";
      let inundaAbout = inunda.shadowRoot.querySelector("#about-linkl");
      inundaAbout.style.display = "none";

      // Abrir o modal quando o icone for clicado master-icon
      let inundaIcon = inunda.shadowRoot.querySelector("#inunda-icon");
      inundaIcon.addEventListener("click", function() {
        let inundaIconPulse = inunda.shadowRoot.querySelector(
          "#inunda-icon-pulse"
        );
        let inundaIcon = inunda.shadowRoot.querySelector("#inunda-icon");
        let inundaModal = inunda.shadowRoot.querySelector("#inunda-form");
        let inundaOverlay = inunda.shadowRoot.querySelector("#inunda-overlay");
        let tooltipModal = inunda.shadowRoot.querySelector("#tooltip");

        var req = http.request(validFunc, function(res) {
          var chunks = [];

          res.on("data", function(chunk) {
            chunks.push(chunk);
          });

          res.on("end", function() {
            var body = Buffer.concat(chunks);
            var infoFromDash = body.toString();
            var newInfoDash = JSON.parse(infoFromDash);
            var { horario, ligacoes, status } = newInfoDash;
            verifyUserStatus(horario, ligacoes, status);
          });
        });

        req.write(JSON.stringify({ token: pluginToken }));
        req.end();

        function verifyUserStatus(horario, ligacoes, status) {
          let myPhoneTab = inunda.shadowRoot.querySelector("#inunda-tabone");
          let tabOne = inunda.shadowRoot.querySelector("#inunda-one");
          let tabTwo = inunda.shadowRoot.querySelector("#inunda-two");
          let tabThree = inunda.shadowRoot.querySelector("#inunda-three");
          let tabFive = inunda.shadowRoot.querySelector("#inunda-five");
          let horarioMsg = horario === "true" ? "msg-on" : "msg-off";
          let userLigacao = ligacoes === "true" ? "call-on" : "call-off";

          if (status !== "bloqueado") {
            if (horarioMsg === "msg-on" && userLigacao === "call-on") {
              myPhoneTab.style.display = "block";
            } else {
              myPhoneTab.style.display = "none";
              tabOne.style.display = "none";
              tabTwo.style.display = "grid";
              tabThree.style.display = "none";
              tabFive.style.display = "none";
            }
          }
        }

        let myMsgTab = inunda.shadowRoot.querySelector("#inunda-tabtwo");
        myMsgTab.style.display = "block";
        tooltipModal.style.display = "none";
        inundaModal.style.display = "block";
        inundaOverlay.style.display = "block";
        inundaIcon.style.display = "none";
        inundaIconPulse.style.display = "none";

        inundaModal.animate(
          [
            //Keyframes
            {
              top: "125%",
              opacity: "0"
            },
            {
              top: "50%",
              opacity: "1"
            }
          ],
          {
            // Timing options
            duration: 520,
            easing: "cubic-bezier(.09,.37,.5,1.46)"
          }
        );

        setTimeout(function() {
          let inundaAbout = inunda.shadowRoot.querySelector("#about-linkl");
          inundaAbout.style.display = "block";
          inundaAbout.animate(
            [
              //Keyframes
              {
                opacity: "0"
              },
              {
                opacity: "1"
              }
            ],
            {
              // Timing options
              duration: 125,
              easing: "cubic-bezier(.09,.37,.5,1.46)"
            }
          );
        }, 625);
      });

      setTimeout(function() {
        let tooltipModal = inunda.shadowRoot.querySelector("#tooltip");
        tooltipModal.style.display = "block";
        let closeTooltip = inunda.shadowRoot.querySelector("#close-tooltip");
        closeTooltip.addEventListener("click", function() {
          let tooltipModal = inunda.shadowRoot.querySelector("#tooltip");
          tooltipModal.style.display = "none";
        });
      }, 5000);

      //Tabs whatsapp inunda-tabfive
      if (pluginWhats === "" || pluginWhats === null) {
        let tabFiveNull = inunda.shadowRoot.querySelector("#inunda-tabfive");
        tabFiveNull.style.display = "none";
      } else {
        let tabFive = inunda.shadowRoot.querySelector("#inunda-tabfive");
        tabFive.addEventListener("click", function() {
          let tabOne = inunda.shadowRoot.querySelector("#inunda-one");
          let tabTwo = inunda.shadowRoot.querySelector("#inunda-two");
          let tabThree = inunda.shadowRoot.querySelector("#inunda-three");
          let tabFive = inunda.shadowRoot.querySelector("#inunda-five");
          let userDateList = inunda.shadowRoot.querySelector(
            "#inunda-datelist"
          );
          userDateList.style.display = "none";
          //let arrowpos = inunda.shadowRoot.querySelector("#trimastercall");
          tabOne.style.display = "none";
          tabTwo.style.display = "none";
          tabThree.style.display = "none";
          tabFive.style.display = "grid";
          // arrowpos.style.display = "block";
        });
      }

      // Modal primeira aba
      let tabOne = inunda.shadowRoot.querySelector("#inunda-tabone");
      tabOne.addEventListener("click", function() {
        let tabOne = inunda.shadowRoot.querySelector("#inunda-one");
        let tabTwo = inunda.shadowRoot.querySelector("#inunda-two");
        let tabThree = inunda.shadowRoot.querySelector("#inunda-three");
        let tabFive = inunda.shadowRoot.querySelector("#inunda-five");
        let userDateList = inunda.shadowRoot.querySelector("#inunda-datelist");
        userDateList.style.display = "none";
        //let arrowpos = inunda.shadowRoot.querySelector("#trimastermsg");

        tabOne.style.display = "grid";
        tabTwo.style.display = "none";
        tabThree.style.display = "none";
        tabFive.style.display = "none";
      });

      // Modal segunda aba
      let tabTwo = inunda.shadowRoot.querySelector("#inunda-tabtwo");
      tabTwo.addEventListener("click", function() {
        let tabOne = inunda.shadowRoot.querySelector("#inunda-one");
        let tabTwo = inunda.shadowRoot.querySelector("#inunda-two");
        let tabThree = inunda.shadowRoot.querySelector("#inunda-three");
        let tabFive = inunda.shadowRoot.querySelector("#inunda-five");
        //let arrowpos = inunda.shadowRoot.querySelector("#trimasteragenda");
        tabOne.style.display = "none";
        tabTwo.style.display = "grid";
        tabThree.style.display = "none";
        tabFive.style.display = "none";
        // arrowpos.style.display = "block";
      });

      // Modal terceira aba
      let tabThree = inunda.shadowRoot.querySelector("#inunda-tabthree");
      tabThree.addEventListener("click", function() {
        let tabOne = inunda.shadowRoot.querySelector("#inunda-one");
        let tabTwo = inunda.shadowRoot.querySelector("#inunda-two");
        let tabThree = inunda.shadowRoot.querySelector("#inunda-three");
        let tabFive = inunda.shadowRoot.querySelector("#inunda-five");
        let userDateList = inunda.shadowRoot.querySelector("#inunda-datelist");
        userDateList.style.display = "none";
        //let arrowpos = inunda.shadowRoot.querySelector("#trimasterwhats");
        tabOne.style.display = "none";
        tabTwo.style.display = "none";
        tabThree.style.display = "grid";
        tabFive.style.display = "none";
        //arrowpos.style.display = "block";
      });

      // inicializa o modal e o overlay com display none
      let inundaModal = inunda.shadowRoot.querySelector("#inunda-form");
      let inundaOverlay = inunda.shadowRoot.querySelector("#inunda-overlay");
      inundaModal.style.display = "none";
      inundaOverlay.style.display = "none";

      // fechar modal quando o overlay for clicado
      inundaOverlay.addEventListener("click", function() {
        let inundaIconPulse = inunda.shadowRoot.querySelector(
          "#inunda-icon-pulse"
        );
        let inundaIcon = inunda.shadowRoot.querySelector("#inunda-icon");
        let inundaModal = inunda.shadowRoot.querySelector("#inunda-form");
        let inundaOverlay = inunda.shadowRoot.querySelector("#inunda-overlay");
        let inundaAbout = inunda.shadowRoot.querySelector("#about-linkl");
        let userDateList = inunda.shadowRoot.querySelector("#inunda-datelist");
        userDateList.style.display = "none";
        inundaAbout.style.display = "none";
        inundaModal.style.display = "none";
        inundaOverlay.style.display = "none";
        inundaIcon.style.display = "block";
        inundaIconPulse.style.display = "block";
        toolTip.style.display = "block";
        // arrowpos.style.display = "block";
      });

      // Fechar o modal quando o x for clicado
      let closeModal = inunda.shadowRoot.querySelector("#inunda-close");
      closeModal.addEventListener("click", function() {
        let inundaIconPulse = inunda.shadowRoot.querySelector(
          "#inunda-icon-pulse"
        );
        let inundaIcon = inunda.shadowRoot.querySelector("#inunda-icon");
        let inundaModal = inunda.shadowRoot.querySelector("#inunda-form");
        let inundaOverlay = inunda.shadowRoot.querySelector("#inunda-overlay");
        let inundaAbout = inunda.shadowRoot.querySelector("#about-linkl");
        let userDateList = inunda.shadowRoot.querySelector("#inunda-datelist");
        userDateList.style.display = "none";
        inundaAbout.style.display = "none";
        inundaModal.style.display = "none";
        inundaOverlay.style.display = "none";
        inundaIcon.style.display = "block";
        inundaIconPulse.style.display = "block";
        //toolTip.style.display = "block";
      });

      // Função pra efetuar ligar onSubmit
      let userCall = inunda.shadowRoot.querySelector("#inunda-submitcall");
      userCall.addEventListener("click", function() {
        let userName = inunda.shadowRoot.querySelector("#inunda-name").value;
        let userPhone = inunda.shadowRoot.querySelector("#inunda-phone").value;

        if (userName.length !== 0 && validator.isMobilePhone(userPhone)) {
          var req = http.request(linklcall, function(res) {
            var chunks = [];

            res.on("data", function(chunk) {
              chunks.push(chunk);
            });

            res.on("end", function() {
              if (tabFour.style.display == "none") {
                setTimeout(function() {
                  let tabOne = inunda.shadowRoot.querySelector("#inunda-one");
                  let tabFour = inunda.shadowRoot.querySelector("#inunda-four");
                  tabOne.style.display = "none";
                  tabFour.style.display = "block";
                  setTimeout(function() {
                    let userNameNull = inunda.shadowRoot.querySelector(
                      "#inunda-name"
                    );
                    let userPhoneNull = inunda.shadowRoot.querySelector(
                      "#inunda-phone"
                    );

                    tabOne.style.display = "grid";
                    tabFour.style.display = "none";

                    userNameNull.value = null;
                    userPhoneNull.value = null;
                  }, 3000);
                }, 2000);
              }
            });
          });

          req.write(
            '{"codigo":"' +
              pluginToken +
              '","nome":"' +
              userName +
              '","fone":"' +
              userPhone +
              '","email":"","agendamento":""}'
          );
          req.end();
        } else {
        }
      });

      let userDate = inunda.shadowRoot.querySelector("#inunda-dateng");
      userDate.addEventListener("click", function() {
        let userDateList = inunda.shadowRoot.querySelector("#inunda-datelist");
        let todayDate = inunda.shadowRoot.querySelector("#todayDate");
        let tomorrowDate = inunda.shadowRoot.querySelector("#tomorrowDate")
          .textContent;
        let dayOne = inunda.shadowRoot.querySelector("#dayOne").textContent;
        let dayTwo = inunda.shadowRoot.querySelector("#dayTwo").textContent;

        (function() {
          var req = http.request(options, function(res) {
            var chunks = [];

            res.on("data", function(chunk) {
              chunks.push(chunk);
            });

            res.on("end", function() {
              var body = Buffer.concat(chunks);
              var objBody = JSON.parse(body);
              var atualHour = objBody.currentDateTime.toString();
              var atualWeekDay = objBody.dayOfTheWeek.toString();
              var atualDay = atualHour
                .slice(0, 10)
                .split("-")
                .reverse()
                .toString()
                .replace(/,/g, "/");
              var atualHr = atualHour
                .slice(11, atualHour.length - 1)
                .split(":");
              var atualHrH = Number(atualHr[0]) - 3;
              var atualHrM = Number(atualHr[1]);
              var autalHnM = atualHrH + ":" + atualHrM;
              hours(atualWeekDay, autalHnM, atualDay);
            });
          });

          req.write(
            JSON.stringify({
              $id: null,
              currentDateTime: null,
              utcOffset: null,
              isDayLightSavingsTime: false,
              dayOfTheWeek: "Wednesday",
              timeZoneName: "BRT",
              currentFileTime: 132078717282921620,
              ordinalDate: "2019-198",
              serviceResponse: null
            })
          );
          req.end();
        })();

        if (userDateList.style.display == "block") {
          userDateList.style.display = "none";
        } else {
          userDateList.style.display = "block";
        }

        function hours(atualWeekDay, autalHnM, atualDay) {
          let today = atualDay;
          let moment = autalHnM;
          let dayOfTheWeek = atualWeekDay;

          todayDate.innerHTML = moment;

          switch (dayOfTheWeek) {
            case "Monday":
              let Monday = dayOfTheWeek.replace("Monday", "Segunda-feira");
            case "Tuesday":
              let Tuesday = dayOfTheWeek.replace("Tuesday", "Terça-feira");
            case "Wednesday":
              let Wednesday = dayOfTheWeek.replace("Wednesday", "Quarta-feira");
            case "Thursday":
              let Thursday = dayOfTheWeek.replace("Thursday", "Quinta-feira");
              console.log(Thursday);
            case "Friday":
              let Friday = dayOfTheWeek.replace("Friday", "Sexta-feira");
            case "Saturnday":
              let Saturnday = dayOfTheWeek.replace("Saturnday", "Sabado");
            case "Sunday":
              let Sunday = dayOfTheWeek.replace("Sunday", "Domingo");
          }
        }
      });

      // Função pra efetuar agendar onSubmit
      let userAg = inunda.shadowRoot.querySelector("#inunda-submitagendar");
      userAg.addEventListener("click", function() {
        let userName = inunda.shadowRoot.querySelector("#inunda-nameng").value;
        let userPhone = inunda.shadowRoot.querySelector("#inunda-phoneng")
          .value;
        let userHour = inunda.shadowRoot.querySelector("#inunda-hourng").value;
        let openMenu = inunda.shadowRoot.querySelector("#inunda-datearrow");

        openMenu.addEventListener("click", function() {});

        if (userName.length !== 0 && validator.isMobilePhone(userPhone)) {
          var req = http.request(linklcall, function(res) {
            var chunks = [];

            res.on("data", function(chunk) {
              chunks.push(chunk);
            });

            res.on("end", function() {
              if (tabFour.style.display == "none") {
                setTimeout(function() {
                  let tabOne = inunda.shadowRoot.querySelector("#inunda-two");
                  let tabFour = inunda.shadowRoot.querySelector("#inunda-four");
                  tabOne.style.display = "none";
                  tabFour.style.display = "block";
                  setTimeout(function() {
                    let userNameNull = inunda.shadowRoot.querySelector(
                      "#inunda-name"
                    );
                    let userPhoneNull = inunda.shadowRoot.querySelector(
                      "#inunda-phone"
                    );

                    tabOne.style.display = "grid";
                    tabFour.style.display = "none";

                    userNameNull.value = null;
                    userPhoneNull.value = null;
                  }, 3000);
                }, 2000);
              }
            });
          });

          req.write(
            '{"codigo":"' +
              pluginToken +
              '","nome":"' +
              userName +
              '","fone":"' +
              userPhone +
              '","email":"' +
              null +
              '","agendamento":"' +
              userDate +
              " " +
              userHour +
              ":00" +
              '"}'
          );
          req.end();
        } else {
        }
      });

      // Função pra enviar mensagem a ligação onSubmit
      let userMsg = inunda.shadowRoot.querySelector("#inunda-submitmsg");
      userMsg.addEventListener("click", function() {
        let userName = inunda.shadowRoot.querySelector("#inunda-namemsg").value;
        let userPhone = inunda.shadowRoot.querySelector("#inunda-phonemsg")
          .value;
        let userEmail = inunda.shadowRoot.querySelector("#inunda-emailmsg")
          .value;
        let userMsg = inunda.shadowRoot.querySelector("#inunda-msgmsg").value;

        if (
          (userName.length !== 0) &
          validator.isEmail(userEmail) &
          validator.isMobilePhone(userPhone)
        ) {
          var req = http.request(linklmsg, function(res) {
            var chunks = [];

            res.on("data", function(chunk) {
              chunks.push(chunk);
            });

            res.on("end", function() {
              if (tabFour.style.display == "none") {
                setTimeout(function() {
                  let tabThree = inunda.shadowRoot.querySelector(
                    "#inunda-three"
                  );
                  let tabFour = inunda.shadowRoot.querySelector("#inunda-four");
                  tabThree.style.display = "none";
                  tabFour.style.display = "block";
                  setTimeout(function() {
                    let userNameNull = inunda.shadowRoot.querySelector(
                      "#inunda-namemsg"
                    );
                    let userPhoneNull = inunda.shadowRoot.querySelector(
                      "#inunda-phonemsg"
                    );
                    let userEmailNull = inunda.shadowRoot.querySelector(
                      "#inunda-emailmsg"
                    );
                    let userMsgNull = inunda.shadowRoot.querySelector(
                      "#inunda-msgmsg"
                    );

                    tabThree.style.display = "grid";
                    tabFour.style.display = "none";

                    userNameNull.value = null;
                    userPhoneNull.value = null;
                    userEmailNull.value = null;
                    userMsgNull.value = null;
                  }, 3000);
                }, 2000);
              }
            });
          });

          req.write(
            '{"codigo":"' +
              pluginToken +
              '","nome":"' +
              userName +
              '","fone":"' +
              userPhone +
              '","email":"' +
              userEmail +
              '","mensagem":"' +
              userMsg +
              '"}'
          );
          req.end();
        } else {
        }
      });

      let userWhats = inunda.shadowRoot.querySelector("#inunda-submitwhats");
      userWhats.addEventListener("click", function() {
        let userLink = inunda.shadowRoot.querySelector("#inunda-submitwhats");
        let userPhoneWhats = inunda.shadowRoot.querySelector(
          "#inunda-phonewhats"
        ).value;
        let userMsgWhats = inunda.shadowRoot.querySelector("#inunda-msgwhats")
          .value;
        let originalPhone = userPhoneWhats;

        console.log(userMsgWhats);

        function _isMobile() {
          var isMobile = /iphone|ipod|android|ie|blackberry|fennec/.test(
            navigator.userAgent.toLowerCase()
          );
          return isMobile;
        }

        if (!_isMobile()) {
          const URL_WHATS_WEB =
            "https://web.whatsapp.com/send?phone=55" +
            pluginWhats +
            "&text=" +
            userMsgWhats;

          userLink.href = URL_WHATS_WEB;
          userLink.target = "_blank";
          userLink.rel = "noopener";
        } else {
          const URL_WHATS_MOB =
            "https://api.whatsapp.com/send?phone=55" +
            pluginWhats +
            "&text=" +
            userMsgWhats;

          userLink.href = URL_WHATS_MOB;
          userLink.target = "_blank";
          userLink.rel = "noopener";
        }

        function cleanNumber(p) {
          let digits = p.replace(/\D/g, "");
          return digits;
        }

        var userWhatsRealNum = cleanNumber(originalPhone);

        if (
          validator.isMobilePhone(userWhatsRealNum) &&
          userMsgWhats.length !== 0
        ) {
          var req = http.request(linklwhats, function(res) {
            var chunks = [];
            res.on("data", function(chunk) {
              chunks.push(chunk);
            });
            res.on("end", function() {
              if (tabFour.style.display == "none") {
                setTimeout(function() {
                  let tabThree = inunda.shadowRoot.querySelector(
                    "#inunda-five"
                  );
                  let tabFour = inunda.shadowRoot.querySelector("#inunda-four");
                  tabThree.style.display = "none";
                  tabFour.style.display = "block";
                  setTimeout(function() {
                    let userPhoneWhats = inunda.shadowRoot.querySelector(
                      "#inunda-phonewhats"
                    );
                    let userMsgWhats = inunda.shadowRoot.querySelector(
                      "#inunda-msgwhats"
                    );

                    tabThree.style.display = "grid";
                    tabFour.style.display = "none";

                    userPhoneWhats.value = null;
                    userMsgWhats.value = null;
                  }, 3000);
                }, 2000);
              }
            });
          });
          req.write(
            '{"codigo":"' +
              pluginToken +
              '" , "nome":"' +
              null +
              '","fone":"' +
              userWhatsRealNum +
              '","mensagem":"' +
              userMsgWhats +
              '"}'
          );
          req.end();
        } else {
        }
      });
    }
  }
}
